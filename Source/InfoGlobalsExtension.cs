using System.Runtime.CompilerServices;
using TenCrowns.GameCore;

namespace FasterFortify {
  public static class InfoGlobalsExtension {
    private static readonly ConditionalWeakTable<InfoGlobals, InfoGlobalsAdditionalData> data =
      new ConditionalWeakTable<InfoGlobals, InfoGlobalsAdditionalData>();

    public static InfoGlobalsAdditionalData GetAdditionalData(this InfoGlobals infoGlobals) {
      return data.GetOrCreateValue(infoGlobals);
    }
  }
}