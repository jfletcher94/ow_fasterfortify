using System;
using CustomXmlTags;
using TenCrowns.GameCore;

namespace FasterFortify {
  [Serializable]
  public class InfoImprovementAdditionalData : IInfoAdditionalData<InfoImprovement> {
    [XmlTag("iFortifyTurnReduction")] public int miFortifyTurnReduction = 0;

    [XmlTag("iFortifyTurnReductionFriendly")]
    public int miFortifyTurnReductionFriendly = 0;
  }
}