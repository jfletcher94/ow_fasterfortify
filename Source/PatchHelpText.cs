using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using HarmonyLib;
using Mohawk.UIInterfaces;
using TenCrowns.ClientCore;
using TenCrowns.GameCore;
using TenCrowns.GameCore.Text;
using UnityEngine;

namespace FasterFortify {
  [HarmonyPatch(typeof(HelpText))]
  public class PatchHelpText {
    [HarmonyPostfix]
    [HarmonyPatch(
        typeof(HelpText), "buildEffectUnitHelp", typeof(TextBuilder), typeof(EffectUnitType), typeof(Game),
        typeof(bool), typeof(bool), typeof(bool))]
    static void PostfixBuildEffectUnitHelp(
        HelpText __instance, TextBuilder builder, EffectUnitType eEffectUnit, Game pGame, bool bSkipIcons,
        bool bRightJustify
    ) {
      int miFortifyTurnReduction =
          __instance.Infos().effectUnit(eEffectUnit).GetAdditionalData().miFortifyTurnReduction;
      int miTestudoTurnReduction =
          __instance.Infos().effectUnit(eEffectUnit).GetAdditionalData().miTestudoTurnReduction;
      if (miFortifyTurnReduction > 0) {
        if (miFortifyTurnReduction == miTestudoTurnReduction) {
          builder.AddTEXT(
              "TEXT_HELPTEXT_EFFECT_UNIT_HELP_FORTIFY_AND_TESTUDO_TURN_REDUCTION_SAME",
              __instance.buildTurnsTextVariable(miFortifyTurnReduction, pGame, false, false),
              TextExtensions.TEXTVAR(bRightJustify));
        } else if (miTestudoTurnReduction > 0) {
          builder.AddTEXT(
              "TEXT_HELPTEXT_EFFECT_UNIT_HELP_FORTIFY_AND_TESTUDO_TURN_REDUCTION_DIFFERENT",
              __instance.buildTurnsTextVariable(miFortifyTurnReduction, pGame, false, false),
              __instance.buildTurnsTextVariable(miTestudoTurnReduction, pGame, false, false),
              TextExtensions.TEXTVAR(bRightJustify));
        } else {
          builder.AddTEXT(
              "TEXT_HELPTEXT_EFFECT_UNIT_HELP_FORTIFY_TURN_REDUCTION",
              __instance.buildTurnsTextVariable(miFortifyTurnReduction, pGame, false, false),
              TextExtensions.TEXTVAR(bRightJustify));
        }
      } else if (miTestudoTurnReduction > 0) {
        builder.AddTEXT(
            "TEXT_HELPTEXT_EFFECT_UNIT_HELP_TESTUDO_TURN_REDUCTION",
            __instance.buildTurnsTextVariable(miTestudoTurnReduction, pGame, false, false),
            TextExtensions.TEXTVAR(bRightJustify));
      }
    }

    [HarmonyPrefix]
    [HarmonyPatch(
        typeof(HelpText), "buildLinkHelp", typeof(TextBuilder), typeof(WidgetData), typeof(ClientManager), typeof(bool),
        typeof(bool), typeof(bool))]
    static bool PrefixBuildLinkHelp(
        HelpText __instance, TextBuilder builder, WidgetData pWidget, ClientManager pManager, bool bName, bool bHelp,
        bool bEncyclopedia, ref TextBuilder __result
    ) {
      if (string.IsNullOrEmpty(pWidget[0]) || pWidget.GetDataEnum<LinkType>(0) != LinkType.HELP_TRADE_NETWORK) {
        return true;
      }

      __result = builder;
      City pCapitalCity = pManager.activePlayer() != null ? pManager.activePlayer().capitalCity() : null;
      builder.AddTEXT("TEXT_HELPTEXT_LINK_HELP_TRADE_NETWORK", __instance.buildCapitalLinkVariable(pCapitalCity));
      if (__instance.Infos().Globals.GetAdditionalData().TRADE_NETWORK_FORTIFY_TURN_REDUCTION != 0) {
        builder.AddTEXT(
            "TEXT_HELPTEXT_LINK_HELP_TRADE_NETWORK_TURN_REDUCTION",
            __instance.buildTurnsTextVariable(
                __instance.Infos().Globals.GetAdditionalData().TRADE_NETWORK_FORTIFY_TURN_REDUCTION,
                pManager.GameClient, false, false));
      }
      __instance.buildDividerText(builder);
      TextVariable hotkeyVariable = __instance.buildHotkeyVariableOrFalse(
          pManager.Interfaces.Hotkeys, __instance.Infos().Globals.HOTKEY_SHOW_TRADE_NETWORK);
      if (hotkeyVariable.IsTrue()) {
        builder.AddTEXT(
            "TEXT_HELPTEXT_CITY_CONNECTED_HOTKEY", hotkeyVariable, __instance.buildCapitalLinkVariable(pCapitalCity));
      }
      return false;
    }

    [HarmonyTranspiler]
    [HarmonyPatch(
        typeof(HelpText), "buildImprovementHelp", typeof(TextBuilder), typeof(ImprovementType), typeof(Tile),
        typeof(ClientManager), typeof(bool), typeof(bool), typeof(bool), typeof(bool), typeof(TextBuilder.ScopeType))]
    static IEnumerable<CodeInstruction> TranspilerBuildImprovement(
        IEnumerable<CodeInstruction> instructions, ILGenerator ilGenerator
    ) {
      MethodInfo helper = AccessTools.Method(typeof(PatchHelpText), "buildImprovementHelpHelper");
      
      // Add transpiled code after code block that contains: 
      // builder.AddTEXT("TEXT_HELPTEXT_IMPROVEMENT_HELP_DEFENSE_MODIFIER_FRIENDLY", ...);
      State state = State.Ready;
      foreach (var instruction in instructions) {
        switch (state) {
          // Finished adding transpiled code
          case State.Finished: {
            yield return instruction;
            break;
          }
          // Waiting to reach code block that immediately precedes where transpiled code will be added
          case State.Ready: {
            if (instruction.opcode == OpCodes.Ldstr
                && "TEXT_HELPTEXT_IMPROVEMENT_HELP_DEFENSE_MODIFIER_FRIENDLY".Equals((string) instruction.operand)) {
              state = State.WithinPrecedingBlock;
            }
            yield return instruction;
            break;
          }
          // Withing preceding code block, waiting for it to finish
          case State.WithinPrecedingBlock: {
            if (instruction.opcode == OpCodes.Pop) {
              state = State.EndOfPrecedingBlock;
            }
            yield return instruction;
            break;
          }
          // End of preceding code block, time to add transpiled code
          case State.EndOfPrecedingBlock: {
            // Preceding code block can be arbitrarily deeply nested within {}
            if (instruction.opcode == OpCodes.Nop) {
              yield return instruction;
              break;
            }

            CodeInstruction start = new CodeInstruction(OpCodes.Ldarg_0);
            instruction.MoveLabelsTo(start);
            yield return start;
            yield return new CodeInstruction(OpCodes.Ldarg_1);
            yield return new CodeInstruction(OpCodes.Ldarg_2);
            yield return new CodeInstruction(OpCodes.Ldarg_S, 4);
            yield return new CodeInstruction(OpCodes.Call, helper);
            yield return new CodeInstruction(OpCodes.Nop);

            yield return instruction;
            state = State.Finished;
            break;
          }
        }
      }
    }

    public static void buildImprovementHelpHelper(
        HelpText helpText, TextBuilder builder, ImprovementType eImprovement, ClientManager pManager
    ) {
      Game pGame = pManager.GameClient;
      {
        int iValue = FasterFortify.ModSettings.Infos.improvement(eImprovement).GetAdditionalData().miFortifyTurnReduction;
        if (iValue != 0) {
          builder.AddTEXT(
              "TEXT_HELPTEXT_IMPROVEMENT_HELP_DEFENSE_MODIFIER",
              helpText.TEXTVAR_TYPE(
                  "TEXT_HELPTEXT_IMPROVEMENT_HELP_FORTIFY_TURN_REDUCTION",
                  helpText.buildTurnsTextVariable(iValue, pGame, false, false)));
        }
      }
      {
        int iValue = FasterFortify.ModSettings.Infos.improvement(eImprovement).GetAdditionalData().miFortifyTurnReductionFriendly;
        
        if (iValue != 0) {
          builder.AddTEXT(
              "TEXT_HELPTEXT_IMPROVEMENT_HELP_DEFENSE_MODIFIER_FRIENDLY",
              helpText.TEXTVAR_TYPE(
                  "TEXT_HELPTEXT_IMPROVEMENT_HELP_FORTIFY_TURN_REDUCTION",
                  helpText.buildTurnsTextVariable(iValue, pGame, false, false)));
        }
      }
    }

    private enum State {
      Ready,
      WithinPrecedingBlock,
      EndOfPrecedingBlock,
      Finished
    }
  }
}