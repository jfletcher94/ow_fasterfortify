using System.Runtime.CompilerServices;
using TenCrowns.GameCore;

namespace FasterFortify {
  public static class InfoImprovementExtension {
    private static readonly ConditionalWeakTable<InfoImprovement, InfoImprovementAdditionalData> data =
      new ConditionalWeakTable<InfoImprovement, InfoImprovementAdditionalData>();

    public static InfoImprovementAdditionalData GetAdditionalData(this InfoImprovement infoImprovement) {
      return data.GetOrCreateValue(infoImprovement);
    }
  }
}