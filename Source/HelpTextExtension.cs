using System.Reflection;
using HarmonyLib;
using TenCrowns.GameCore;

namespace FasterFortify {
  public static class HelpTextExtension {
    public static MethodInfo infos = AccessTools.Method(typeof(HelpText), "infos");

    public static Infos Infos(this HelpText helpText) {
      return (Infos) infos.Invoke(helpText, null);
    }
  }
}