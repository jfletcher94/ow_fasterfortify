using System;
using CustomXmlTags;
using TenCrowns.GameCore;

namespace FasterFortify {
  [Serializable]
  public class InfoGlobalsAdditionalData : IInfoAdditionalData<InfoGlobals> {
    [XmlTag] public int TRADE_NETWORK_FORTIFY_TURN_REDUCTION = 0;
  }
}