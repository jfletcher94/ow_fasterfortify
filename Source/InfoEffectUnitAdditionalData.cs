using System;
using CustomXmlTags;
using TenCrowns.GameCore;

namespace FasterFortify {
  [Serializable]
  public class InfoEffectUnitAdditionalData : IInfoAdditionalData<InfoEffectUnit> {
    [XmlTag("iFortifyTurnReduction")] public int miFortifyTurnReduction = 0;
    [XmlTag("iTestudoTurnReduction")] public int miTestudoTurnReduction = 0;
  }
}