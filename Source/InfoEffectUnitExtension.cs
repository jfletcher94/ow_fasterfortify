using System.Runtime.CompilerServices;
using TenCrowns.GameCore;

namespace FasterFortify {
  public static class InfoEffectUnitExtension {
    private static readonly ConditionalWeakTable<InfoEffectUnit, InfoEffectUnitAdditionalData> data =
      new ConditionalWeakTable<InfoEffectUnit, InfoEffectUnitAdditionalData>();

    public static InfoEffectUnitAdditionalData GetAdditionalData(this InfoEffectUnit infoEffectUnit) {
      return data.GetOrCreateValue(infoEffectUnit);
    }
  }
}