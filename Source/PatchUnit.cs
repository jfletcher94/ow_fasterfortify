using HarmonyLib;
using TenCrowns.GameCore;

namespace FasterFortify {
  [HarmonyPatch(typeof(Unit))]
  public class PatchUnit {
    [HarmonyPrefix]
    [HarmonyPatch(typeof(Unit), "changeFortifyTurns", typeof(int))]
    static bool PrefixChangeFortifyTurns(Unit __instance, int iChange) {
      int maxFortifyTurns = __instance.game().infos().Globals.MAX_FORTIFY_TURNS;
      // Only take effect if unit is already fortified but not at max value, and fortify value is increasing
      // (This is a proxy for checking if the fortify level is increasing at the start of the turn)
      if (iChange <= 0 || __instance.getFortifyTurns() <= 0 || __instance.getFortifyTurns() >= maxFortifyTurns) {
        return true;
      }

      int turnReduction = 0;
      Tile tile = __instance.tile();
      if (tile.onTradeNetworkCapital(__instance.getPlayer())) {
        turnReduction += __instance.game().infos().Globals.GetAdditionalData().TRADE_NETWORK_FORTIFY_TURN_REDUCTION;
      }
      if (tile.hasImprovementFinished()) {
        InfoImprovementAdditionalData data =
          __instance.game().infos().improvement(tile.getImprovement()).GetAdditionalData();
        if (!tile.hasOwner()) {
          turnReduction += data.miFortifyTurnReduction;
        } else if (!__instance.game().isHostileUnit(tile.getTeam(), TribeType.NONE, __instance)) {
          turnReduction += data.miFortifyTurnReduction + data.miFortifyTurnReductionFriendly;
        }
      }
      for (int i = 0; i < __instance.getEffectUnits().Count; i++) {
        EffectUnitType effectUnitType = __instance.getEffectUnits()[i];
        turnReduction +=
          __instance.game().infos().effectUnit(effectUnitType).GetAdditionalData().miFortifyTurnReduction;
      }

      // There is no fortify turn reduction for this unit, or it is not fully fortified even accounting for turn reduction
      if (turnReduction <= 0 || __instance.getFortifyTurns() + iChange < maxFortifyTurns - turnReduction) {
        return true;
      }

      // Unit is fully fortified, accounting for fortify turn reduction
      __instance.setFortifyTurns(maxFortifyTurns);
      return false;
    }

    [HarmonyPrefix]
    [HarmonyPatch(typeof(Unit), "changeTestudoTurns", typeof(int))]
    static bool PrefixChangeTestudoTurns(Unit __instance, int iChange) {
      int maxTestudoTurns = __instance.game().infos().Globals.MAX_TESTUDO_TURNS;
      // Only take effect if unit is already in testudo but not at max value, and testudo value is increasing
      // (This is a proxy for checking if the testudo level is increasing at the start of the turn)
      if (iChange <= 0 || __instance.getTestudoTurns() <= 0 || __instance.getTestudoTurns() >= maxTestudoTurns) {
        return true;
      }

      int turnReduction = 0;
      for (int i = 0; i < __instance.getEffectUnits().Count; i++) {
        EffectUnitType effectUnitType = __instance.getEffectUnits()[i];
        turnReduction +=
          __instance.game().infos().effectUnit(effectUnitType).GetAdditionalData().miTestudoTurnReduction;
      }

      // There is no testudo turn reduction for this unit, or it is not fully fortified even accounting for turn reduction
      if (turnReduction <= 0 || __instance.getTestudoTurns() + iChange < maxTestudoTurns - turnReduction) {
        return true;
      }

      // Unit is fully fortified, accounting for fortify turn reduction
      __instance.setTestudoTurns(maxTestudoTurns);
      return false;
    }
  }
}