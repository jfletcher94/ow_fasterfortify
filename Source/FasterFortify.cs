﻿using HarmonyLib;
using TenCrowns.AppCore;
using TenCrowns.GameCore;

namespace FasterFortify {
  public class FasterFortify : ModEntryPointAdapter {
    public const string MY_HARMONY_ID = "Thremtopod.FasterFortify.patch";
    public static Harmony harmony;
    public static ModSettings ModSettings { get; private set; }

    public override void Initialize(ModSettings modSettings) {
      if (harmony != null) {
        return;
      }
      CustomXmlTags.CustomXmlTags.AppendTags<InfoEffectUnit, InfoEffectUnitAdditionalData>(
          unit => unit.GetAdditionalData());
      CustomXmlTags.CustomXmlTags.AppendTags<InfoImprovement, InfoImprovementAdditionalData>(
          improvement => improvement.GetAdditionalData());
      CustomXmlTags.CustomXmlTags.AppendGlobals<InfoGlobalsAdditionalData>(globals => globals.GetAdditionalData());
      harmony = new Harmony(MY_HARMONY_ID);
      harmony.PatchAll();
      ModSettings = modSettings;
    }

    public override void Shutdown() {
      if (harmony == null) {
        return;
      }
      harmony.UnpatchAll(MY_HARMONY_ID);
      harmony = null;
      ModSettings = null;
    }
  }
}